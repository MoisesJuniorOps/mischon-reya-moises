from collections import Counter

def word_frequency(input_file, output_file):
    with open(input_file) as f:
        text = f.read().lower().split()

    word_freq = Counter(text)

    with open(output_file, 'w') as f:
        for word, freq in word_freq.items():
            f.write(f"{word}: {freq}\n")

word_frequency("file_input.txt", "file_output.txt")