import java.util.ArrayList;
import java.util.List;

public class FruitBowl {
    private static FruitBowl instance;
    private List<String> fruits;

    private FruitBowl() {
        fruits = new ArrayList<>();
    }

    public static FruitBowl getInstance() {
        if (instance == null) {
            instance = new FruitBowl();
        }
        return instance;
    }

    public void addFruit(String fruit) {
        fruits.add(fruit);
    }

    public void displayFruits() {
        System.out.println("Fruits added to the bowl:");
        for (String fruit : fruits) {
            System.out.println("- " + fruit);
        }
    }
}
