import os
import requests

# Created function to retrieve events of an user's calendar.Needs to export the variable of the token.
def get_calendar_events():
    access_token = os.environ.get('MS_GRAPH_ACCESS_TOKEN')
    if not access_token:
        print("Access token not found. Make sure to set the MS_GRAPH_ACCESS_TOKEN environment variable.")
        return None
    
    url = "https://graph.microsoft.com/v1.0/me/calendar/events"
    headers = {
        "Authorization": "Bearer " + access_token
    }
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        print("Failed to retrieve calendar events:", response.text)
        return None

#Usage 
events = get_calendar_events()
if events:
    print("Calendar Events:")
    for event in events['value']:
        print("- " + event['subject'])
